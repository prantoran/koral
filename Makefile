EXECUTABLE_NAME=KORAL
CPP=g++
NVCC=nvcc
ARCH=sm_75
GENCODE=compute_75,code=sm_75
INC=-I/usr/local/cuda/include/
NVCCFLAGS=-Wall -Wextra -Wshadow -Ofast -fomit-frame-pointer -mavx2 -mfma -funroll-all-loops -fpeel-loops -ftracer -ftree-vectorize -D_MWAITXINTRIN_H_INCLUDED -fopenmp
CPPFLAGS=-Wall -Wextra -Wshadow -pedantic -Ofast -std=gnu++14 -fomit-frame-pointer -march=native -flto -funroll-all-loops -fpeel-loops -ftracer -ftree-vectorize -mavx2 -mfma
LIBS=-L/usr/local/cuda/lib64 -lcudart -lpthread -lopencv_core -lopencv_features2d -lopencv_highgui -lopencv_imgcodecs
CPPSOURCES=$(wildcard *.cpp)
CUSOURCES=$(wildcard *.cu)

OBJECTS=$(CPPSOURCES:.cpp=.o) $(CUSOURCES:.cu=.o)

all:$(CPPSOURCES) $(CUSOURCES) $(EXECUTABLE_NAME)

	# echo CPPSOURCES: $(CPPSOURCES)
	# echo CUSOURCES: $(CUSOURCES)
	# echo EXECUTABLE_NAME: $(EXECUTABLE_NAME)
	
$(EXECUTABLE_NAME) : $(OBJECTS)
	$(CPP) $(CPPFLAGS) $(OBJECTS) -o $@ $(LIBS)

%.o:%.cpp
	$(CPP) -c $(INC) $(CPPFLAGS) $< -o $@

%.o:%.cu
	$(NVCC) -g -G --use_fast_math -arch=$(ARCH) -gencode arch=${GENCODE} -O3 -ccbin $(CC) -std=c++11 -c $(INC) -Xcompiler "$(NVCCFLAGS)" $< -o $@

clean:
	rm -rf *.o $(EXECUTABLE_NAME)


debug:
	CUDA_DEBUGGER_SOFTWARE_PREEMPTION=1 cuda-gdb ./KORAL