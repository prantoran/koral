## Notes
__global__

<<blocks, threads>>

cudaDeviceSynchronize();

nvprof ./...

gridDim.x
	the number of blocks in the grid

blockIdx.x
	the index of the current thread block in the grid

blockDim.x
	the number of threads in the block

threadIdx.x
	the index of the current thread within its block

__CUDA_ARCH__
	used to differentiate various code paths based on compute capability.
	defined for device code.

driver API

nvcc -m64
	The 32-bit version of nvcc can compile device code in 64-bit mode also using the -m64 compiler option. 

nvcc -m32
	The 64-bit version of nvcc can compile device code in 32-bit mode also using the -m32 compiler option. 

cudart library
	implements cuda runtime
	linked to application either 
		statically
			via cudart.lib or libcudart.a
		dynamically
			via cudart.dll or libcudart.so	


cuda runtime
	shared memory
	page-locked host memory
	asynchronous concurrent execution
	multi-device system
	error checking 
	call stack
	texture and surface memory
	graphics interoperablity
	
cuda context
	per device
	primary context for each device
	initialized at the first runtime function call which require an active context
	shared among all the host threads of the application
	the primary context of a device can be accessed from the driver API


cudaDeviceReset()
	destroys the primary context of the device the host thread currently operates on 
		The next runtime function call made by any host thread that has this 
		device as current will create a new primary context for this device. 


Note: The CUDA interfaces use global state that is initialized during host program initiation and destroyed during host program termination. The CUDA runtime and driver cannot detect if this state is invalid, so using any of these interfaces (implicitly or explicity) during program initiation or termination after main) will result in undefined behavior. 

Device memory can be allocated either as linear memory or as CUDA arrays. 
CUDA arrays are opaque memory layouts optimized for texture fetching


Linear memory is allocated in a single unified address space, which means that separately allocated entities can reference one another via pointers, for example, in a binary tree or linked list. The size of the address space depends on the host system (CPU) and the compute capability of the used GPU

cudaMalloc()
	allocates linear memory

cudaFree()
	frees allocated linear memory

cudaMemcpy()
	data transfer between host memory and device memory
		cudaMemcpyHostToDevice
		cudaMemcpyDeviceToHost


	cudaMallocPitch()
		linear memory allocations of 2D arrays
		makes sure that the allocation is appropriately padded to meet the alignment requirements
			ensuring best performance when accessing the row addresses or performing copies between 2D arrays and other regions of device memory (using the cudaMemcpy2D() and cudaMemcpy3D() functions)

	cudaMalloc3D()
		linear memory allocations of 3D arrays
		makes sure that the allocation is appropriately padded to meet the alignment requirements
			ensuring best performance when accessing the row addresses or performing copies between 2D arrays and other regions of device memory (using the cudaMemcpy2D() and cudaMemcpy3D() functions)

	The returned pitch (or stride) must be used to access array elements.


Note: To avoid allocating too much memory and thus impacting system-wide performance, request the allocation parameters from the user based on the problem size. If the allocation fails, you can fallback to other slower memory types (cudaMallocHost(), cudaHostRegister(), etc.), or return an error telling the user how much memory was needed that was denied. If your application cannot request the allocation parameters for some reason, we recommend using cudaMallocManaged() for platforms that support it. 


Texture memory
- handles filtering, bounce checking, etc
- data should be copied to CUDA arrays
- A block of read-only memory shared by all multi-processors
	1D, 2D, or 3D array
	Texels: Up to 4-element vectors
		x, y, z, w
- Reads from texture memory can be “samples” of multiple texels
- Slow to access
	several hundred clock cycle latency
- But it is cached: 
	8KB per multi-processor
	Fast access if cache hit
- Good for random accesses to a large read-only data structure
- Texture fetches are cached
	Optimized for 2D locality
- Addressing
	1D, 2D, or 3D 
- Coordinates
	integer or normalized
	Fewer addressing calculations in code
- Provide filtering for free
- Free out-of-bounds handling: wrap modes
	Clamp to edge / warp
- Limitations of CUDA textures:
	Read-only from within a kernel
- Regular Indexing
	- Indexes are floating point numbers
		Think of the texture as a surface as opposed to a grid for which you have a grid of samples
- Normalized Indexing
	- NxM Texture: 
		[0,1.0) x [0.0, 1.0) indexes
	- Convenient if you want to express the computation in size-independent terms
- What Value Does a Texture Reference Return?
	Nearest-Point Sampling 
		- Comes for “free”
		- Elements must be floats
			- (1.0, 1.0) = (1, 1)
			  (0.9, 0.9) = (0, 0)
			  (2.5, 2.5) = (2, 2)
		- In this filtering mode, the value returned by the texture fetch is
			tex(x) = T[i] for a one-dimensional texture,
			tex(x, y) = T[i, j] for a two-dimensional texture,
			tex(x, y, z) = T[i, j, k] for a three-dimensional texture,
				where i = floor(x) , j = floor( y) , and k = floor(z) .
		- 1D texture behaves more like a conventional array	
	Linear Filtering
		(1.0, 1.0) = 1/4(0,0) + 1/4(0,1) + 1/4(1,0) + 1/4(1,1)
		(2.5, 2.5) = (2,2)
		alpha, beta, gamma are stored in 9-bit fixed point format with 8 bits of fractional value
		Effectively the value read is a weighted average of all neighboring texels
- Dealing with Out-of-Bounds References
	- clamping
		Get’s stuck at the edge
			i < 0  actual i = 0
			i > N -1  actual i = N -1
	- Warping
		Warps around
			actual i = i MOD N
			Useful when texture is a periodic signal
- Texels
	- All elemental datatypes
		- All elemental datatypes
			-  Integer, char, short, float (unsigned)
		- CUDA vectors: 1, 2, or 4 elements
			- char1, uchar1, char2, uchar2,
			- char4, uchar4, short1, ushort1, short2, ushort2,
			- short4, ushort4, int1, uint1,
			- int2, uint2, int4, uint4, long1,
			- ulong1, long2, ulong2, long4,
			- ulong4, float1, float2, float4, 
- Programmer’s view of Textures
	- Texture Reference Object
		- Use that to access the elements
		- Tells CUDA what the texture looks like
	- Space to hold the values
		- Linear Memory (portion of memory)
			- Only for 1D textures
		- CUDA Array
			- Special CUDA Structure used for Textures
				- Opaque
		- Bind the two:
			- Space and Reference
- Texture Reference Object
	- ReadMode
		- What values are returned
			- cudaReadModeElementType
				- Just the elements -> What you write is what you get
			- cudaReadModeNormalizedFloat
				- Works for chars and shorts (unsigned)
				- valid for 8- or 16-bit ints
				- Value normalized to [0.0, 1.0]
- CUDA Containers: Linear Memory
	- Global memory is bound to a texture
		- CudaMalloc()
	- Only 1D
	- Integer addressing
	- No filtering, no addressing modes
	- Return either element type or normalized float
- CUDA Containers: CUDA Arrays
	- CUDA array is bound to a texture
	- 1D, 2D, or 3D
	- Float addressing 
		- size-based, normalized
	- Filtering
	- Addressing modes
		- clamping, warping
	- Return either element type or normalized float
- CUDA Array Type
	Channel format, width, height
	cudaChannelFormatDesc structure
		int x, y, z, w: parts for each component
		enum cudaChannelFormatKind – one of:
			cudaChannelFormatKindSigned
			cudaChannelFormatKindUnsigned
			cudaChannelFormatKindFloat
		Some predefined constructors:
			cudaCreateChannelDesc<float>(void);
			cudaCreateChannelDesc<float4>(void);
	Management functions:
		cudaMallocArray, cudaFreeArray,
		cudaMemcpyToArray, cudaMemcpyFromArray, ...
- Textures are Optimized for 2D Locality
	- Regular Array Allocation
		- Row-Major
	- Because of Filtering
		- Neighboring texels
		- Accessed close in time
	- Texture blocking
		- 
- Using Textures
	- Textures are read-only
		- Within a kernel
	- A kernel can produce an array
		- Cannot write CUDA Arrays
	- Then this can be bound to a texture for the next kernel
	- Linear Memory can be copied to CUDA Arrays
		- cudaMemcpyFromArray()
			- Copies linear memory array to a CudaArray
		- cudaMemcpyToArray()
			- Copies CudaArray to linear memory array
- Pitch:
	- Padding added to achieve good locality
	- Some functions require this pitch to be passed as a an argument
	- Prefer those that use it from the Array structure directly
- padded at the end, into to align warps at the end of rows
	- similar concept to cudaMallocPitch(), cudaMemcpy2D()
	
constant memory
	read-only for kernels
	cached on the SM
	optimized for 2D locality


occupancy
	keeping alll SMs busy

warp size
	 the number of threads that a multiprocessor executes concurrently. An NVIDIA multiprocessor can execute several threads from the same block at the same time, using hardware multithreading.

CUDA API Errors
	c-style approach with status codes
	cudaError_t
		75 error types
	Helper functions
		cudaGetErrorString()
		cudaGetLastError()
	

NVIDIA Visual Profiler
	trace device code and CUDA API calls
	Guided analysis

coalesced memory access
	adjacent threads combine into one transaction
	

instruction-level parallelism
	SMs can run multiple independent instructions at onces
	can hide memory latency (400+ cycles)
	can hide arithmetic latency (~20 cycles)
	compute multiple independent resutls per thread


Warp Divergence
	BAD - performance pitfall
	entire warp must run the same instruction
	if and loop statements can cause divergence
	some threads are left idel while others run


Intrinsic functions
	fast, low-precision versions of floating-point functions
	division, trigonometry, log, exp, pow, and so on
	named with leading __, for example, __sinf()
	enable everywhere with --use_fast_math


Concurrency in CUDA
	multiple concurrent kernels
	host execution concurrent with kernels
	memory transfers overlapping with computation

CUDA Streams
	independent sequences of commands
	within a stream - ordered execution
	between streams - concurrent execution
	default stream runs all commands sequentially

Synchronization
	cudaDeviceSynchronize(): wait for all streams
	cudaStreamSynchronize(): wait for one stream
	cudaStreamQuery(): check stream status
		check whether stream has finished without waiting


Listing devices
	cudaGetDeviceCount(): get the number of devices
	cudaGetDeviceProperties(): Get the device properties
	cudaDeviceProps struct: details about a particular device


selecting devices
	cudaSetDevice(): set the current device
	cudaGetDevic(): get the index of the current device
	cudaChooseDevice(): select a device based on its properties
	

multiple cuda devices
	separate streams 
	separate memory allocations
	must launch lernels on the correct device
	

unified virtual addressing
	all the cuda-allocated host and device memory is in one address space
	can determine the memory location from it address
	usa cudaMemcpyDefault for copies
	use cudaPointerGetAttributes() to query pointer info
	
	
	
dynamic global memory allocation
	allocation from device code
		usually allocate with a host API: cudaMalloc() and cudaFree()
		can also allocate in device code
		c++ style: new and delete
		c style: malloc() and free()
			
	do not mix new/delete with malloc/free
	separate heap from the host API
	do not mix device host and device allocation
