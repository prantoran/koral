FROM nvidia/cuda:10.2-devel-ubuntu16.04

# Dockerfile adapted from https://qiita.com/kndt84/items/9524b1ab3c4df6de30b8

ENV DEBIAN_FRONTEND noninteractive

ARG OPENCV_VERSION='3.4.9'
ARG GPU_ARCH='7.5'

WORKDIR /opt

# Build tools
RUN apt update && \
    apt install -y \
    sudo \
    tzdata \
    git \
    cmake \
    cmake-curses-gui \
    wget \
    curl \
    unzip \
    build-essential

# Media I/O:
RUN apt install -y \
    zlib1g-dev \
    libjpeg-dev \
    libwebp-dev \
    libpng-dev \
    libtiff5-dev \
    libopenexr-dev \
    libgdal-dev \
    libgtk2.0-dev

# Video I/O:
RUN apt install -y \
    libdc1394-22-dev \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libtheora-dev \
    libvorbis-dev \
    libxvidcore-dev \
    libx264-dev \
    yasm \
    libopencore-amrnb-dev \
    libopencore-amrwb-dev \
    libv4l-dev \
    libxine2-dev \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev

# Parallelism and linear algebra libraries:
RUN apt install -y \
    libtbb-dev \
    libeigen3-dev

# Python:
RUN apt install -y \
    python3-dev \
    python3-tk \
    python3-numpy

# Build OpenCV
RUN wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip && \
    unzip ${OPENCV_VERSION}.zip && rm ${OPENCV_VERSION}.zip && \
    mv opencv-${OPENCV_VERSION} OpenCV && \
    cd OpenCV && \
    mkdir build && \
    cd build && \
    cmake \
    -D WITH_TBB=ON \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D BUILD_EXAMPLES=ON \
    -D WITH_FFMPEG=ON \
    -D WITH_V4L=ON \
    -D WITH_OPENGL=ON \
    -D WITH_CUDA=ON \
    -D CUDA_ARCH_BIN=${GPU_ARCH} \
    -D CUDA_ARCH_PTX=${GPU_ARCH} \
    -D WITH_CUBLAS=ON \
    -D WITH_CUFFT=ON \
    -D WITH_EIGEN=ON \
    -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
    -D CUDA_CUDA_LIBRARY=/usr/local/cuda/lib64/stubs/libcuda.so \
    .. && \
    make all -j$(nproc) && \
    make install



# GCC7
RUN apt-get install -y software-properties-common \
    && add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt update \
    && apt install g++-7 -y \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 \
    --slave /usr/bin/g++ g++ /usr/bin/g++-7 \
    && update-alternatives --config gcc

# VCPKG
RUN git clone https://github.com/Microsoft/vcpkg.git \
    && cd vcpkg \
    && ./bootstrap-vcpkg.sh \
    && ./vcpkg integrate install \
    && ln -s /opt/vcpkg/vcpkg /usr/bin/vcpkg

# CMAKE 3.17
RUN wget https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0.tar.gz \
    && tar xvfz cmake-3.17.0.tar.gz \
    && cd cmake-3.17.0 \
    && ./configure \
    && make -j$(nproc) \
    && make install

ENV CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH}:/opt/vcpkg/installed/x64-linux/share

# canberra-gtk-module for cv::imshow()
RUN apt install libcanberra-gtk-module libcanberra-gtk3-module -y

ENV PATH="${PATH}:/usr/local/cuda-10.2/bin"

RUN echo "PATH:"$PATH
# if runfile used for cuda install
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/usr/local/cuda-10.2/lib64"
RUN echo "LD_LIBRARY_PATH:" $LD_LIBRARY_PATH


RUN mkdir -p /opt/dev/koral
RUN ls -la
RUN echo "curdir before changin workdir:" `pwd`

WORKDIR /opt/dev/koral
RUN echo "cuurr dir:" `pwd`


CMD nvidia-smi

# RUN echo "nvidia driver version: " && \
#     nvidia-smi

RUN echo "nvcc location: " && \
    which nvcc && \
    echo "nvcc version: " && \
    nvcc --version

COPY . /opt/dev/koral
# COPY CLATCH.cu Makefile /opt/dev/koral
# ADD ./* /opt/dev/koral
# ADD CLATCH.cu Makefile /opt/dev/koral/

RUN echo "curdir:" `pwd` && \
    ls -la && \
    make -j$(nproc)


RUN echo "updated directory contents:\n"
RUN ls
# RUN sudo ./main.o


# expose port 8080 to have it mapped by Docker daemon
EXPOSE 8080

# KORAL-ROS-POSTGRES
# RUN git clone --recurse-submodules https://bitbucket.org/dammian_miller/koral.git \
#     && cd koral \
#     && make -j$(nproc)


# WORKDIR /opt/koral
SHELL ["/bin/bash", "-c"]
CMD ["/bin/bash"]
